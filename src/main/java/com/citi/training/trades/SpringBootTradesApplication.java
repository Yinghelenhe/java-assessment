package com.citi.training.trades;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootTradesApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootTradesApplication.class, args);
	}

}
