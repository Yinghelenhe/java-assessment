package com.citi.training.trades.rest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.trades.model.Trades;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("h2")
public class TradesControllerIntegrationTests {

    private static final Logger LOG = LoggerFactory.getLogger(
                                            TradesControllerIntegrationTests.class);

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void getStock_returnsStock() {
    	
        String testStock = "MSFT";
        int testBuy = 1;
        double testPrice = 110;
        int testVolume = 123;
        

        ///Do a HTTP POST to /stocks
        ResponseEntity<Trades> createTradesResponse = restTemplate.postForEntity("/trades",
                                             new Trades(1, testStock, testBuy, 
                                            		 testPrice, testVolume),
                                             Trades.class);
       

        LOG.info("Create Trades response: " + createTradesResponse.getBody());
        assertEquals(HttpStatus.CREATED, createTradesResponse.getStatusCode());
        assertEquals("created trades stock should equal test stock",
                testStock, createTradesResponse.getBody().getStock());
        assertEquals("created trades buy should equal test buy",
                testBuy, createTradesResponse.getBody().getBuy());
        assertEquals("created trades volume should equal test vloume",
                testPrice, createTradesResponse.getBody().getPrice(),0.001);
        assertEquals("created trades volume should equal test vloume",
                testVolume, createTradesResponse.getBody().getVolume());
        
        ///Do a HTTP GET to /trades
        ResponseEntity<Trades> findTradesResponse = restTemplate.getForEntity(
        										"/trades/" + createTradesResponse.getBody().getId(),
                                                Trades.class);

        LOG.info("FindbyId response: " + findTradesResponse.getBody());
        assertEquals(HttpStatus.OK, findTradesResponse.getStatusCode());
        assertEquals("created trades stock should equal test stock",
                testStock, findTradesResponse.getBody().getStock());
        assertEquals("created trades buy should equal test buy",
                testBuy, findTradesResponse.getBody().getBuy());
        assertEquals("created trades volume should equal test vloume",
                testPrice, findTradesResponse.getBody().getPrice(),0.001);
        assertEquals("created trades volume should equal test vloume",
                testVolume, findTradesResponse.getBody().getVolume());
        
        //Do a HTTP DELET to /trades
        
        restTemplate.delete(
				"/trades/" + findTradesResponse.getBody().getId()
                );


        findTradesResponse = restTemplate.getForEntity(
        										"/trades/" + createTradesResponse.getBody().getId(),
                                                Trades.class);

        LOG.info("FindbyId response: " + findTradesResponse.getBody());
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, findTradesResponse.getStatusCode());
        
        
        
        
    }
}
