package com.citi.training.trades.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

//“stock” is a stock symbol e.g. AAPL, GOOGL, MSFT
// “buy” is an integer value, which will be either 1 or 0.
//o 1 indicates this trade is a buy.
//o 0 indicates this trade is a sell.
// “price” is a dollar amount at which this trade will be made
// “volume” is an integer number of stocks to be traded 

@Entity
public class Trades {
	
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    private String stock;
    private int buy;
    private double price;
    private int volume;

    public Trades() {}

    public Trades(int id, String stock, int buy, double price, int volume) {
        this.id = id;
        this.stock = stock;
        this.buy = buy;
        this.price = price;
        this.volume = volume;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStock() {
    	return stock;
    }
    
    public void setStock(String stock) {
    	this.stock = stock;
    }
    
    public int getBuy() {
    	return buy;
    }
    
    public void setBuy(int buy) {
    	this.buy = buy;
    }
    
    public double getPrice() {
    	return price;
    }
    
    public void setPrice(double price) {
    	this.price = price;
    }
    
    public int getVolume() {
    	return volume;
    }
    
    public void setVolume(int volume) {
    	this.volume = volume;
    }

    @Override
    public String toString() {
        return "Trades [id=" + id + ", stock=" + stock + ", buy=" + buy +
        		",price=" + price + ", volume=" + volume + "]";
    }

    public boolean equals(Trades trades) {
        return this.id == trades.getId() &&
               this.stock.equals(trades.getStock()) &&
               this.price == trades.getPrice() &&
               this.buy == trades.getBuy() &&
               this.volume ==  trades.getVolume();
    }
}
