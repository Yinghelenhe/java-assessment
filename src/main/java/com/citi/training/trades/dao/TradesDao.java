package com.citi.training.trades.dao;

import org.springframework.data.repository.CrudRepository;

import com.citi.training.trades.model.Trades;

public interface TradesDao extends CrudRepository<Trades, Integer> {}
