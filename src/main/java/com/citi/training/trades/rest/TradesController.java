package com.citi.training.trades.rest;
import com.citi.training.trades.dao.TradesDao;
import com.citi.training.trades.model.Trades;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/trades")
public class TradesController {
	
	//o List<Stock> findAll() 
	// Stock findById(@PathVariable long id)
	// void create (@RequestBody Stock stock)
	// @ResponseBody void deleteById(@PathVariable long id) 
	
	private static final Logger LOG = 
			LoggerFactory.getLogger(TradesController.class);

    @Autowired
    TradesDao tradesdao;
	
	@RequestMapping(method=RequestMethod.GET)
    public Iterable<Trades> findAll(){
    	LOG.info("HTTP GET to findAll()");
    	LOG.debug("This is a much more verbose debugging message!");
        return tradesdao.findAll();
    }
	
	@RequestMapping(value="/{id}",method =RequestMethod.GET)
	public Trades findById(@PathVariable int id) {
	    	return tradesdao.findById(id).get();
	    }
	
	 @RequestMapping(method=RequestMethod.POST)
	 public HttpEntity<Trades> save (@RequestBody Trades trades) {
	    	tradesdao.save(trades);
	    	return new ResponseEntity<Trades>(trades, HttpStatus.CREATED);
	    }
	 
	 @RequestMapping(value="/{id}",method =RequestMethod.DELETE)
	 public void remove(@PathVariable int id) {
	    	tradesdao.deleteById(id);
	    }
	 
	 

}
