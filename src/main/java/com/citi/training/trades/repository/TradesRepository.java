package com.citi.training.trades.repository;

import org.springframework.data.repository.CrudRepository;

import com.citi.training.trades.model.Trades;

public interface TradesRepository extends CrudRepository<Trades, Integer>{

  
}
