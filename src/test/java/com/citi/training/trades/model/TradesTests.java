package com.citi.training.trades.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TradesTests {
	

    private int testId = 1;
    private String testStock = "AAPL";
    private int testBuy = 1;
    private double testPrice = 599.49;
    private int testVolume = 112;

    @Test
    public void test_Trades_defaultConstructorAndSetters() {
    	
        Trades testTrades = new Trades();

        testTrades.setId(testId);
        testTrades.setStock(testStock);
        testTrades.setPrice(testPrice);
        testTrades.setBuy(testBuy);
        testTrades.setVolume(testVolume);
        

        assertEquals("Trades id should be equal to testId",
                     testId, testTrades.getId());
        assertEquals("Trades stock should be equal to testStock",
                     testStock, testTrades.getStock());
        assertEquals("Trades price should be equal to testPrice",
                     testPrice, testTrades.getPrice(), 0.00001);
        assertEquals("Trades buy should be equal to testbuy",
                testBuy, testTrades.getBuy());
        assertEquals("Trades price should be equal to testVolume",
                testVolume, testTrades.getVolume());
        
    }

    @Test
    public void test_Product_fullConstructor() {
        Trades testTrades = new Trades(testId, testStock, testBuy, testPrice, testVolume);

        assertEquals("Trdes id should be equal to testId",
                     testId, testTrades.getId());
        assertEquals("Trades stock should be equal to testStock",
                     testStock, testTrades.getStock());
        assertEquals("Trades price should be equal to testPrice",
                     testPrice, testTrades.getPrice(), 0.00001);
        assertEquals("Trades buy should be equal to testBuy",
                testBuy, testTrades.getBuy());
        assertEquals("Trades volume should be equal to testVolume",
                testVolume, testTrades.getVolume());
        
    }

    @Test
    public void test_Trades_toString() {
        Trades testTrades = new Trades(testId, testStock, testBuy, testPrice, testVolume);

        assertTrue("toString should contain testId",
        		testTrades.toString().contains(Integer.toString(testId)));
        assertTrue("toString should contain testStock",
        		testTrades.toString().contains(testStock));
        assertTrue("toString should contain testPrice",
        		testTrades.toString().contains(Double.toString(testPrice)));
        assertTrue("toString should contain testBuy",
        		testTrades.toString().contains(Integer.toString(testBuy)));
        assertTrue("toString should contain testVolume",
        		testTrades.toString().contains(Integer.toString(testVolume)));
    }

    @Test
    public void test_Trades_equals() {
        Trades firstTrades = new Trades(testId, testStock, testBuy, testPrice, testVolume);

        Trades compareTrades = new Trades(testId, testStock, testBuy, testPrice, testVolume);
        assertFalse("Trades for test should not be the same object",
                    firstTrades == compareTrades);
        assertTrue("Trades for test should be found to be equal",
                   firstTrades.equals(compareTrades));
    }

    @Test
    public void test_Trades_equalsFails() {
    	
    	Trades firstTrades = new Trades(testId +1 , testStock, testBuy, testPrice, testVolume);
    	Trades secondTrades = new Trades(testId, testStock + "_test", testBuy, testPrice, testVolume);
    	Trades thirdTrades = new Trades(testId, testStock, testBuy, testPrice*99.99, testVolume);
    	Trades fourthTrades = new Trades(testId, testStock, testBuy-1, testPrice, testVolume);
    	Trades fifthTrades = new Trades(testId, testStock, testBuy, testPrice, testVolume*11);
        
    	Trades[] createdTrades = {firstTrades,secondTrades,thirdTrades,
    			fourthTrades,fifthTrades};
       

        Trades compareTrades = new Trades(testId, testStock, testBuy, testPrice, testVolume);

        for(Trades thisTrades: createdTrades) {
            assertFalse("Trades for test should not be the same object",
                        thisTrades == compareTrades);
            assertFalse("Trades for test should not be found to be equal",
                        thisTrades.equals(compareTrades));
        }
    }
}
